package nl.willemvandam.vodagone.dao;

public interface ITokenDao
{
    void deleteExpiredTokens();

    void insertOrUpdateToken(String token, int abonneeId);

    int getIdByToken(String token);
}
