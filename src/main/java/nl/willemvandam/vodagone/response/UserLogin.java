package nl.willemvandam.vodagone.response;

// Used to catch user login values in the form request
public class UserLogin
{
    private String user;
    private String password;

    public UserLogin()
    {
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getUser()
    {
        return this.user;
    }
}
