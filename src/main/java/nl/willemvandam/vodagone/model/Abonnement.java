package nl.willemvandam.vodagone.model;

public class Abonnement
{
    private static final String VERDUBBELD = "verdubbeld";
    private static final String STANDAARD = "standaard";
    private static final String NIET_BESCHIKBAAR = "niet-beschikbaar";
    private static final String OPGEZEGD = "opgezegd";
    private static final String PRICE_WITHOUT_DECIMAL = "€%.0f,- per maand";
    private static final String PRICE_WITH_DECIMAL = "€%.2f per maand";
    private static final double MULTIPLIER_FOR_VERDUBBELD = 1.5;
    private final int id;
    private final String startDatum;
    private final String verdubbeling;
    private final String status;
    private final boolean eigenaar;
    private final Dienst dienst;

    public Abonnement(int id, String startDatum, boolean verdubbeld, String status, boolean eigenaar, Dienst dienst)
    {
        this.id = id;
        this.startDatum = startDatum;
        if (dienst.getVerdubbelbaar())
        {
            if (verdubbeld)
            {
                this.verdubbeling = VERDUBBELD;
            }
            else
            {
                this.verdubbeling = STANDAARD;
            }
        }
        else
        {
            this.verdubbeling = NIET_BESCHIKBAAR;
        }
        this.status = status;
        this.eigenaar = eigenaar;
        this.dienst = dienst;
    }

    public int getId()
    {
        return id;
    }

    public String getAanbieder()
    {
        return dienst.getAanbieder();
    }

    public String getDienstName()
    {
        return dienst.getDienst();
    }

    public double getPrijs()
    {
        double prijs = 0.00;
        if (!this.status.equals(OPGEZEGD))
        {
            switch (verdubbeling)
            {
                case VERDUBBELD:
                    prijs += dienst.getPrijsPerMaand() * MULTIPLIER_FOR_VERDUBBELD;
                    break;
                default:
                    prijs += dienst.getPrijsPerMaand();
                    break;
            }
        }
        return prijs;
    }

    public String getPrijsInString()
    {
        double prijs = this.getPrijs();
        if (prijs % 1 == 0)
        {
            return String.format(PRICE_WITHOUT_DECIMAL, prijs);
        }
        return String.format(PRICE_WITH_DECIMAL, prijs);
    }

    public String getStartDatum()
    {
        return startDatum;
    }

    public String getVerdubbeling()
    {
        return this.verdubbeling;
    }

    public boolean getDeelbaar()
    {
        return this.eigenaar && dienst.getDeelbaar() > 0;
    }

    public String getStatus()
    {
        return this.status;
    }

    public boolean isEigenaar()
    {
        return this.eigenaar;
    }
}
