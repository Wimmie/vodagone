package nl.willemvandam.vodagone.integration;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class integrationDienstenTest
{
    private String token;

    @Before
    public void init()
    {
        RestAssured.port = 8080;

        String json = "{\"user\": \"w.vandam2@student.han.nl\",\"password\": \"1995-12-06\"}";

        // Get the token of the user
        token =
                given().
                        accept(ContentType.JSON).
                        contentType(ContentType.JSON).
                        body(json).
                when().
                        post("/login").
                then().
                extract().
                        path("token");
    }

    @Test
    public void shouldReturnForbiddenForBadToken()
    {
        given().
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                get("/abonnementen/all");
    }

    @Test
    public void shouldReturnEmptyListForFilter()
    {
        given().
                contentType(ContentType.JSON).
                queryParam("token", token).
                queryParam("filter", "very big filter which will return no result").
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/all").
        then().
                body("size()", is(0));

    }

    @Test
    public void shouldReturnDienstenListForNoFilter()
    {
        given().
                contentType(ContentType.JSON).
                queryParam("token", token).
                queryParam("filter", "").
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/all").
        then().
                body(matchesJsonSchemaInClasspath("diensten-schema.json"));

    }
}
