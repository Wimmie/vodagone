package nl.willemvandam.vodagone.integration;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import nl.willemvandam.vodagone.response.AbonnementId;
import nl.willemvandam.vodagone.response.DienstResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class integrationAbonneesTest
{
    private String token;

    @Before
    public void init()
    {
        RestAssured.port = 8080;

        // Get the token of the user
        String json = "{\"user\": \"w.vandam2@student.han.nl\",\"password\": \"1995-12-06\"}";

        // Get the token of the user
        token =
                given().
                        accept(ContentType.JSON).
                        contentType(ContentType.JSON).
                        body(json).
                when().
                        post("/login").
                then().
                extract().
                        path("token");
    }

    @Test
    public void shouldReturnForbiddenForBadToken()
    {
        given().
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                get("/abonnees");
    }

    @Test
    public void shouldReturnOtherAbonneesForAbonneesWithToken()
    {
        given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnees").
        then().
                body("name", not(hasItem("Willem van Dam"))).
                body("id", not(hasItem(1))).
                body("email", not(hasItem("w.vandam2@student.han.nl"))).
                body(matchesJsonSchemaInClasspath("abonnees-schema.json"));
    }

    @Test
    public void shouldReturnForbiddenForBadTokenShareAbonnement()
    {
        String abonnementIdJson = "{\"id\": 1}";
        given().
                contentType(ContentType.JSON).
                pathParam("id", 2).
                queryParam("token", "token").
                body(abonnementIdJson).
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                post("/abonnees/{id}");
    }

    @Test
    public void shouldReturnOkForShareableAbonnement()
    {
        // Insert an abonnement that can be shared
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(6, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen");

        // Get the ID of the inserted abonnement
        ArrayList<Integer> ids = given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen").
        then().
        extract().
                path("abonnementen.id");
        int id = ids.get(ids.size() - 1);

        // Check that the abonnement shareable
        given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("deelbaar", is(true));

        // Share the abonnement
        String abonnementIdJson = String.format("{\"id\": %1$d}", id);
        given().
                contentType(ContentType.JSON).
                pathParam("id", 2).
                queryParam("token", token).
                body(abonnementIdJson).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                post("/abonnees/{id}");
    }

    @Test
    public void shouldReturnBadRequestForMaxSharedAbonnement()
    {
        // Insert an abonnement that can be shared
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(6, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen");

        // Get the ID of the inserted abonnement
        ArrayList<Integer> ids = given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen").
        then().
        extract().
                path("abonnementen.id");
        int id = ids.get(ids.size() - 1);

        // Check that the abonnement shareable
        given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("deelbaar", is(true));

        // Share the abonnement
        String abonnementIdJson = String.format("{\"id\": %1$d}", id);
        given().
                contentType(ContentType.JSON).
                pathParam("id", 2).
                queryParam("token", token).
                body(abonnementIdJson).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                post("/abonnees/{id}");

        // Check that the abonnement shareable
        given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("deelbaar", is(true));

        // Share the abonnement again
        given().
                contentType(ContentType.JSON).
                pathParam("id", 3).
                queryParam("token", token).
                body(abonnementIdJson).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                post("/abonnees/{id}");

        // Check that the abonnement is not shareable
        given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("deelbaar", is(false));

        // Share the abonnement again but fail
        given().
                contentType(ContentType.JSON).
                pathParam("id", 4).
                queryParam("token", token).
                body(abonnementIdJson).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                post("/abonnees/{id}");
    }
}
