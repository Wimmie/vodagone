/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     28-3-2018 23:21:42                           */
/*==============================================================*/

DROP TABLE IF EXISTS AANBIEDER;

DROP TABLE IF EXISTS ABONNEE;

DROP TABLE IF EXISTS ABONNEMENT;

DROP TABLE IF EXISTS ABONNEMENTLENGTE;

DROP TABLE IF EXISTS ABONNEMENTSTATUS;

DROP TABLE IF EXISTS DIENST;

DROP TABLE IF EXISTS GEDEELDABONNEMENT;

DROP TABLE IF EXISTS PRIJZEN;

DROP TABLE IF EXISTS TOKEN;

DROP TABLE IF EXISTS USER;

/*==============================================================*/
/* Table: AANBIEDER                                             */
/*==============================================================*/
CREATE TABLE AANBIEDER
(
  NAAM VARCHAR(256) NOT NULL,
  PRIMARY KEY (NAAM)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: ABONNEE                                               */
/*==============================================================*/
CREATE TABLE ABONNEE
(
  ID    INT          NOT NULL AUTO_INCREMENT,
  NAME  VARCHAR(256) NOT NULL,
  EMAIL VARCHAR(256) NOT NULL,
  PRIMARY KEY (ID),
  KEY AK_KEY_2 (EMAIL)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: ABONNEMENT                                            */
/*==============================================================*/
CREATE TABLE ABONNEMENT
(
  ID         INT          NOT NULL AUTO_INCREMENT,
  ABONNEE    INT          NOT NULL,
  STATUS     VARCHAR(256) NOT NULL,
  DIENST     INT          NOT NULL,
  LENGTE     VARCHAR(255) NOT NULL,
  STARTDATUM DATE         NOT NULL,
  EINDDATUM  DATE,
  VERDUBBELD BOOLEAN      NOT NULL,
  PRIMARY KEY (ID)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: ABONNEMENTLENGTE                                      */
/*==============================================================*/
CREATE TABLE ABONNEMENTLENGTE
(
  LENGTE VARCHAR(255) NOT NULL,
  PRIMARY KEY (LENGTE)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: ABONNEMENTSTATUS                                      */
/*==============================================================*/
CREATE TABLE ABONNEMENTSTATUS
(
  STATUS VARCHAR(256) NOT NULL,
  PRIMARY KEY (STATUS)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: DIENST                                                */
/*==============================================================*/
CREATE TABLE DIENST
(
  NAAM          VARCHAR(256) NOT NULL,
  DEELBAAR      INT          NOT NULL,
  VERDUBBELBAAR BOOLEAN      NOT NULL,
  AANBIEDER     VARCHAR(256) NOT NULL,
  ID            INT          NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (ID)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: GEDEELDABONNEMENT                                     */
/*==============================================================*/
CREATE TABLE GEDEELDABONNEMENT
(
  ABONNEEID    INT NOT NULL,
  ABONNEMENTID INT NOT NULL,
  PRIMARY KEY (ABONNEEID, ABONNEMENTID)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: PRIJZEN                                               */
/*==============================================================*/
CREATE TABLE PRIJZEN
(
  DIENST INT          NOT NULL,
  LENGTE VARCHAR(255) NOT NULL,
  PRIJS  DOUBLE       NOT NULL,
  PRIMARY KEY (DIENST, LENGTE)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: TOKEN                                                 */
/*==============================================================*/
CREATE TABLE TOKEN
(
  TOKEN   VARCHAR(19) NOT NULL,
  ID      INT         NOT NULL,
  EXPIRES DATETIME    NOT NULL,
  PRIMARY KEY (TOKEN),
  UNIQUE KEY AK_Key_2 (ID)
)
  ENGINE = InnoDB;

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/
CREATE TABLE USER
(
  USERNAME VARCHAR(256) NOT NULL,
  ID       INT          NOT NULL,
  PASSWORD VARCHAR(256) NOT NULL,
  PRIMARY KEY (USERNAME)
)
  ENGINE = InnoDB;

ALTER TABLE ABONNEMENT
  ADD CONSTRAINT FK_ABONNEE_IN_ABONNEMENT FOREIGN KEY (ABONNEE)
REFERENCES ABONNEE (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE ABONNEMENT
  ADD CONSTRAINT FK_DIENST_IN_ABONNEMENT FOREIGN KEY (DIENST)
REFERENCES DIENST (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE ABONNEMENT
  ADD CONSTRAINT FK_LENGTE_VAN_ABONNEMENT FOREIGN KEY (LENGTE)
REFERENCES ABONNEMENTLENGTE (LENGTE)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE ABONNEMENT
  ADD CONSTRAINT FK_STATUS_IN_ABONNNEMENT FOREIGN KEY (STATUS)
REFERENCES ABONNEMENTSTATUS (STATUS)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE DIENST
  ADD CONSTRAINT FK_AANBIEDER_IN_DIENST FOREIGN KEY (AANBIEDER)
REFERENCES AANBIEDER (NAAM)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE GEDEELDABONNEMENT
  ADD CONSTRAINT FK_ABONNEMENT FOREIGN KEY (ABONNEMENTID)
REFERENCES ABONNEMENT (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE GEDEELDABONNEMENT
  ADD CONSTRAINT FK_GEBRUIKER FOREIGN KEY (ABONNEEID)
REFERENCES ABONNEE (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE PRIJZEN
  ADD CONSTRAINT FK_PRIJS_VOOR_LENGTE FOREIGN KEY (LENGTE)
REFERENCES ABONNEMENTLENGTE (LENGTE)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE PRIJZEN
  ADD CONSTRAINT FK_PRIJS_IN_DIENST FOREIGN KEY (DIENST)
REFERENCES DIENST (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE TOKEN
  ADD CONSTRAINT FK_TOKEN_OF_ABONNEE FOREIGN KEY (ID)
REFERENCES ABONNEE (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE USER
  ADD CONSTRAINT FK_USER_IN_ABONNEE FOREIGN KEY (ID)
REFERENCES ABONNEE (ID)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

