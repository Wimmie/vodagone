package nl.willemvandam.vodagone.model;

public class Dienst
{
    private final int id;
    private final String dienst;
    private final String aanbieder;
    private final double prijsPerMaand;
    private final int deelbaar;
    private final boolean verdubbelbaar;

    public Dienst(int id, String aanbieder, String dienst, double prijsPerMaand, int deelbaar, boolean verdubbelbaar)
    {
        this.id = id;
        this.aanbieder = aanbieder;
        this.dienst = dienst;
        this.prijsPerMaand = prijsPerMaand;
        this.deelbaar = deelbaar;
        this.verdubbelbaar = verdubbelbaar;
    }

    public int getId()
    {
        return id;
    }

    public String getAanbieder()
    {
        return aanbieder;
    }

    public String getDienst()
    {
        return dienst;
    }

    public double getPrijsPerMaand()
    {
        return prijsPerMaand;
    }

    // Returns the amount of users the abonnement can be shared with
    public int getDeelbaar()
    {
        return deelbaar;
    }

    public boolean getVerdubbelbaar()
    {
        return verdubbelbaar;
    }
}