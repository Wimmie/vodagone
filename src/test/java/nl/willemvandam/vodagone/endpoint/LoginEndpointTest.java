package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.dao.IAbonneeDao;
import nl.willemvandam.vodagone.model.Abonnee;
import nl.willemvandam.vodagone.model.User;
import nl.willemvandam.vodagone.response.UserLogin;
import nl.willemvandam.vodagone.response.UserWithTokenResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginEndpointTest
{
    @Mock
    private IAbonneeDao mockAbonneeDao;

    @InjectMocks
    private LoginEndpoint loginEndpoint;

    @Mock
    private UserLogin mockUserLogin;

    @Mock
    private User mockUser;

    @Test
    public void shouldReturnUnauthorizedForLogin()
    {
        Response response = loginEndpoint.login(new UserLogin());

        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnCreatedForLogin()
    {
        when(mockUserLogin.getPassword()).thenReturn("password");
        when(mockUserLogin.getUser()).thenReturn("user");
        when(mockAbonneeDao.getUser(anyString(), anyString())).thenReturn(mockUser);

        Response response = loginEndpoint.login(mockUserLogin);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnUserWithTokenResponseForLogin()
    {
        Abonnee abonnee = new Abonnee(0, "Henk", "henk@westbroek.nl");
        User user = new User(abonnee, "token");

        when(mockUserLogin.getPassword()).thenReturn("");
        when(mockUserLogin.getUser()).thenReturn("");
        when(mockAbonneeDao.getUser(anyString(), anyString())).thenReturn(user);

        Response response = loginEndpoint.login(mockUserLogin);

        assertEquals(UserWithTokenResponse.class, response.getEntity().getClass());
    }
}