package nl.willemvandam.vodagone.response;

import nl.willemvandam.vodagone.model.Abonnement;

public class AbonnementOfUserResponse
{
    private final int id;
    private final String aanbieder;
    private final String dienst;

    public AbonnementOfUserResponse(Abonnement abonnement)
    {
        this.id = abonnement.getId();
        this.aanbieder = abonnement.getAanbieder();
        this.dienst = abonnement.getDienstName();
    }
}
