package nl.willemvandam.vodagone.integration;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class integrationLoginTest
{
    @Before
    public void init()
    {
        RestAssured.port = 8080;
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test
    public void shouldReturnUserWithTokenResponseJson()
    {
        String json = "{\"user\": \"w.vandam2@student.han.nl\",\"password\": \"1995-12-06\"}";

        String token =
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(json).
        when().
                post("/login").
        then().
                body("user", equalTo("Willem van Dam")).
        extract().
                path("token");

        Assert.assertEquals(19, token.length());
    }
}
