package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IAbonneeDao;
import nl.willemvandam.vodagone.response.AbonnementId;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/abonnees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbonneesEndpoint
{
    @Inject
    private IAbonneeDao abonneeDao;

    @Inject
    private Auth auth;

    @Path("")
    @GET
    public Response getAllOtherAbonnees(@QueryParam("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        return Response.ok()
                .entity(abonneeDao.getAllOtherAbonnees(auth.getUserId()))
                .build();
    }

    @Path("/{id}")
    @POST
    public Response shareAbonnementenById(@PathParam("id") int abonneeId, AbonnementId abonnementId, @QueryParam
            ("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();
        boolean success = abonneeDao.shareAbonnement(auth.getUserId(), abonneeId, abonnementId.getId());

        if (!success)
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.ok()
                .build();
    }
}
