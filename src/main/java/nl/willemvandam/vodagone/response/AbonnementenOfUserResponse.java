package nl.willemvandam.vodagone.response;

import nl.willemvandam.vodagone.model.Abonnement;

import java.util.ArrayList;

public class AbonnementenOfUserResponse
{
    private final ArrayList<AbonnementOfUserResponse> abonnementen;
    private double totalPrice;

    public AbonnementenOfUserResponse(ArrayList<Abonnement> abonnementen)
    {
        this.abonnementen = new ArrayList<>();
        this.totalPrice = 0.00;
        for (Abonnement abonnement : abonnementen)
        {
            if (abonnement.isEigenaar())
            {
                this.totalPrice += abonnement.getPrijs();
            }
            this.abonnementen.add(new AbonnementOfUserResponse(abonnement));
        }
    }
}