package nl.willemvandam.vodagone.dao;

import nl.willemvandam.vodagone.model.Abonnement;

import java.util.ArrayList;

public interface IAbonnementenDao
{
    ArrayList<Abonnement> getAllAbonnementen(int id);

    Abonnement getAbonnemtenById(int userId, int id);

    boolean addAbonnement(int userId, int id);

    void updateAbonnementen();

    boolean verdubbelAbonnement(int userId, int id);

    boolean terminateAbonnement(int userId, int id);
}
