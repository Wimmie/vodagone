package nl.willemvandam.vodagone.model;

import java.util.Random;

public class Token
{
    private final static int PARTS = 4;
    private static final int PART_MAX_VALUE = 9999;
    private final static int PART_MAX_LENGTH = Integer.toString(PART_MAX_VALUE).length();
    private static final String SEPARATOR = "-";

    public static String generateToken()
    {
        StringBuilder token = new StringBuilder();
        for (int i = 0; i < PARTS; i++)
        {
            token.append(generatePart());
            if (i != PARTS - 1)
            {
                token.append(SEPARATOR);
            }
        }
        return token.toString();
    }

    private static String generatePart()
    {
        Random rand = new Random();
        int partAsInt = rand.nextInt(PART_MAX_VALUE) + 1;
        StringBuilder part = new StringBuilder(Integer.toString(partAsInt));

        for (int i = part.length(); i < PART_MAX_LENGTH; i++)
        {
            part.insert(0, "0");
        }

        return part.toString();
    }
}
