package nl.willemvandam.vodagone.authentication;

import nl.willemvandam.vodagone.dao.ITokenDao;
import nl.willemvandam.vodagone.dao.implementation.TokenDao;

import javax.inject.Inject;

public class Auth
{
    @Inject
    private ITokenDao tokenDao;

    private int userId;

    public boolean isAuthorized(String token)
    {
        this.userId = tokenDao.getIdByToken(token);

        return this.userId >= 0;
    }

    public int getUserId()
    {
        return this.userId;
    }
}
