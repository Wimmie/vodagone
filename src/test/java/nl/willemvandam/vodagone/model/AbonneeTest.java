package nl.willemvandam.vodagone.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbonneeTest
{
    private Abonnee abonnee;

    @Before
    public void init()
    {
        this.abonnee = new Abonnee(0, "Henk", "henk@westbroek.nl");
    }

    @Test
    public void shouldReturnName()
    {
        assertEquals("Henk", this.abonnee.getName());
    }

    @Test
    public void shouldReturnId()
    {
        assertEquals(0, this.abonnee.getId());
    }
}