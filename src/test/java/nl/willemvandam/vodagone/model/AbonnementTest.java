package nl.willemvandam.vodagone.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbonnementTest
{
    private final double DELTA = 0.001;
    private Abonnement abonnement;
    private Abonnement abonnementVerdubbeld;
    private Abonnement abonnementOpgezegd;
    private Abonnement abonnementNietDeelbaar;


    @Before
    public void init()
    {
        Dienst dienst = new Dienst(0, "ziggo", "Internet", 5.00, 2, true);
        Dienst dienstNietDeelbaar = new Dienst(0, "ziggo", "Internet", 5.00, 0, true);
        this.abonnement = new Abonnement(0, "2018-03-27", false, "actief", true, dienst);
        this.abonnementVerdubbeld = new Abonnement(0, "2018-03-27", true, "actief", true, dienst);
        this.abonnementOpgezegd = new Abonnement(0, "2018-03-27", false, "opgezegd", true, dienst);
        this.abonnementNietDeelbaar = new Abonnement(0, "2018-03-27", false, "opgezegd", true, dienstNietDeelbaar);
    }

    @Test
    public void shouldReturnAbonnementId()
    {
        assertEquals(0, this.abonnement.getId());
    }

    @Test
    public void shouldReturnAanbieder()
    {
        assertEquals("ziggo", this.abonnement.getAanbieder());
    }

    @Test
    public void shouldReturnDienstName()
    {
        assertEquals("Internet", this.abonnement.getDienstName());
    }

    @Test
    public void shouldReturnPrijsPerMaand()
    {
        assertEquals(5.00, this.abonnement.getPrijs(), DELTA);
    }

    @Test
    public void shouldReturnPrijsPerMaandVerdubbeld()
    {
        assertEquals(7.50, this.abonnementVerdubbeld.getPrijs(), DELTA);
    }

    @Test
    public void shouldReturnNothingForPriceOpgezegd()
    {
        assertEquals(0.00, this.abonnementOpgezegd.getPrijs(), DELTA);
    }

    @Test
    public void shouldReturnPrijsInString()
    {
        assertEquals("€5,- per maand", this.abonnement.getPrijsInString());
    }

    @Test
    public void shouldReturnVerdubbeldPrijsInString()
    {
        assertEquals("€7,50 per maand", this.abonnementVerdubbeld.getPrijsInString());
    }

    @Test
    public void shouldReturnZeroPrijsInString()
    {
        assertEquals("€0,- per maand", this.abonnementOpgezegd.getPrijsInString());
    }

    @Test
    public void shouldReturnStartDatum()
    {
        assertEquals("2018-03-27", this.abonnement.getStartDatum());
    }

    @Test
    public void shouldReturnStandaardForVerdubbeling()
    {
        assertEquals("standaard", this.abonnement.getVerdubbeling());
    }

    @Test
    public void shouldReturnVerdubbeldForVerdubbeling()
    {
        assertEquals("verdubbeld", this.abonnementVerdubbeld.getVerdubbeling());
    }

    @Test
    public void shouldTrueForDeelbaar()
    {
        assertEquals(true, this.abonnement.getDeelbaar());
    }

    @Test
    public void shouldFalseForDeelbaar()
    {
        assertEquals(false, this.abonnementNietDeelbaar.getDeelbaar());
    }

    @Test
    public void shouldReturnActief()
    {
        assertEquals("actief", this.abonnement.getStatus());
    }

    @Test
    public void shouldReturnOpgezegd()
    {
        assertEquals("opgezegd", this.abonnementOpgezegd.getStatus());
    }

    @Test
    public void shouldReturnIsEigenaar()
    {
        assertEquals(true, this.abonnement.isEigenaar());
    }
}