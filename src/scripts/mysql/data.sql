DELETE FROM GedeeldAbonnement;
DELETE FROM Abonnement;
DELETE FROM Token;
DELETE FROM AbonnementStatus;
DELETE FROM Prijzen;
DELETE FROM Dienst;
DELETE FROM AbonnementLengte;
DELETE FROM Aanbieder;
DELETE FROM User;
DELETE FROM Abonnee;

ALTER TABLE User
  AUTO_INCREMENT = 1;
ALTER TABLE Abonnee
  AUTO_INCREMENT = 1;
ALTER TABLE Abonnement
  AUTO_INCREMENT = 1;
ALTER TABLE Dienst
  AUTO_INCREMENT = 1;

INSERT INTO Abonnee (name, email) VALUES ('Willem van Dam', 'w.vandam2@student.han.nl');
INSERT INTO Abonnee (name, email) VALUES ('Meron', 'Meron.Brouwer@han.nl');
INSERT INTO Abonnee (name, email) VALUES ('Dennis', 'Dennis.Breuker@han.nl');
INSERT INTO Abonnee (name, email) VALUES ('Michel', 'Michel.Portier@han.nl');
INSERT INTO User VALUES ('w.vandam2@student.han.nl', 1, '1995-12-06');

INSERT INTO Aanbieder VALUES ('vodafone'), ('ziggo');

INSERT INTO AbonnementLengte VALUES ('maand'), ('jaar'), ('half jaar');

INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder) VALUES ('Mobiele telefonie 100', 0, FALSE, 'vodafone');
INSERT INTO Prijzen VALUES (1, 'maand', 5.00), (1, 'half jaar', 25.00), (1, 'jaar', 45.00);
INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder) VALUES ('Mobiele telefonie 250', 0, TRUE, 'vodafone');
INSERT INTO Prijzen VALUES (2, 'maand', 10.00), (2, 'half jaar', 50.00), (2, 'jaar', 90.00);
INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder)
VALUES ('Glasvezel-internet (download 500Mbps)', 0, TRUE, 'vodafone');
INSERT INTO Prijzen VALUES (3, 'maand', 40.00), (3, 'half jaar', 200.00), (3, 'jaar', 360.00);
INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder)
VALUES ('Kabel-internet (download 300Mbps)', 0, FALSE, 'ziggo');
INSERT INTO Prijzen VALUES (4, 'maand', 30.00), (4, 'half jaar', 150.00), (4, 'jaar', 270.00);
INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder)
VALUES ('Eredivisie Live 1,2,3,4 en 5', 2, FALSE, 'ziggo');
INSERT INTO Prijzen VALUES (5, 'maand', 10.00), (5, 'half jaar', 50.00), (5, 'jaar', 90.00);
INSERT INTO Dienst (naam, deelbaar, verdubbelbaar, aanbieder) VALUES ('HBO Plus', 2, FALSE, 'ziggo');
INSERT INTO Prijzen VALUES (6, 'maand', 15.00), (6, 'half jaar', 75.00), (6, 'jaar', 135.00);

INSERT INTO AbonnementStatus VALUES ('proef'), ('actief'), ('opgezegd');

INSERT INTO Abonnement (abonnee, status, dienst, lengte, startDatum, verdubbeld) VALUES
  (1, 'actief', 1, 'maand', '2018-01-27', FALSE),
  (1, 'actief', 2, 'maand', '2018-01-27', FALSE),
  (1, 'actief', 4, 'maand', '2018-01-27', FALSE),
  (2, 'actief', 5, 'maand', '2018-01-27', FALSE),
  (2, 'actief', 6, 'maand', '2018-01-27', FALSE);

INSERT INTO GedeeldAbonnement VALUES (1, 4);
INSERT INTO GedeeldAbonnement VALUES (3, 5);