package nl.willemvandam.vodagone.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDatabase
{
    void printSQLExeption(SQLException ex);

    void connectAndSelect(String query) throws SQLException;

    boolean connectAndManipulate(String query);

    void closeConnection();

    ResultSet getResult();
}
