package nl.willemvandam.vodagone.dao.implementation;

import nl.willemvandam.vodagone.dao.IAbonnementenDao;
import nl.willemvandam.vodagone.dao.IDatabase;
import nl.willemvandam.vodagone.model.Abonnement;
import nl.willemvandam.vodagone.model.Dienst;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static java.lang.String.format;

public class AbonnementenDao implements IAbonnementenDao
{
    @Inject
    private IDatabase db;

    private static final String QUERY_ALL_ABONNEMENTEN = "SELECT a.ID as abonnementId, a.STARTDATUM, a.VERDUBBELD, " +
            "a.STATUS, (SELECT a.abonnee = %1$d) as eigenaar, d.id as dienstId, d.AANBIEDER, d.NAAM, (SELECT p.PRIJS " +
            "FROM prijzen p WHERE p.DIENST = d.ID AND p.LENGTE = 'maand') prijsPerMaand, (SELECT d.DEELBAAR - (SELECT" +
            " COUNT(*) FROM gedeeldabonnement g WHERE g.ABONNEMENTID = a.ID)) as deelbaar, d.VERDUBBELBAAR FROM " +
            "abonnement a INNER JOIN dienst d ON d.id = a.DIENST WHERE a.ABONNEE = %1$d OR a.id = (SELECT g" +
            ".ABONNEMENTID FROM gedeeldabonnement g WHERE g.ABONNEEID = %1$d)";
    private static final String QUERY_ABONNEMENT_BY_ID = "SELECT a.ID as abonnementId, a.STARTDATUM, a.VERDUBBELD, a" +
            ".STATUS, (SELECT a.abonnee = %1$d) as eigenaar, d.id as dienstId, d.AANBIEDER, d.NAAM, (SELECT p.PRIJS " +
            "FROM prijzen p WHERE p.DIENST = d.ID AND p.LENGTE = 'maand') prijsPerMaand, (SELECT d.DEELBAAR - (SELECT" +
            " COUNT(*) FROM gedeeldabonnement g WHERE g.ABONNEMENTID = a.ID)) as deelbaar, d.VERDUBBELBAAR FROM " +
            "abonnement a INNER JOIN dienst d ON d.ID = a.DIENST WHERE a.ID = %2$d AND (a.ABONNEE = %1$d OR a.ABONNEE" +
            " IN (SELECT a.ABONNEE FROM abonnement a2 WHERE a2.ID IN (SELECT g.ABONNEMENTID FROM gedeeldabonnement g " +
            "WHERE g.ABONNEEID = %1$d AND g.ABONNEMENTID = %2$d)))";
    private static final String QUERY_INSERT_ABONNEMENT = "INSERT INTO abonnement (ABONNEE, STATUS, DIENST, LENGTE, " +
            "STARTDATUM, VERDUBBELD) VALUES (%1$d, 'proef', %2$d, 'maand', NOW(), false)";
    private static final String QUERY_UPDATE_ABONNEMENT_TO_PROEF = "UPDATE abonnement SET STATUS = 'actief' WHERE " +
            "STATUS = 'proef' AND DATEDIFF(NOW(), STARTDATUM) >= 31;";
    private static final String QUERY_UPDATE_ABONNEMENT_TO_OPGEZEGD = "UPDATE abonnement SET STATUS = 'opgezegd' " +
            "WHERE EINDDATUM IS NOT NULL AND STATUS != 'opgezegd' AND DATEDIFF(NOW(), EINDDATUM) > 0;";
    private static final String QUERY_VERDUBBEL_ABONNEMENT = "UPDATE abonnement a SET a.VERDUBBELD = true WHERE a.ID " +
            "= %2$d AND a.ABONNEE = %1$d AND (SELECT d.VERDUBBELBAAR FROM dienst d WHERE d.ID = a.DIENST) = TRUE";
    private static final String QUERY_TERMINATE_ABONNEMENT = "UPDATE abonnement SET STATUS = 'opgezegd' WHERE ID = " +
            "%2$d AND ABONNEE = %1$d";

    @Override
    public ArrayList<Abonnement> getAllAbonnementen(int userId)
    {
        ArrayList<Abonnement> abonnementen = new ArrayList<>();

        try
        {
            this.db.connectAndSelect(format(QUERY_ALL_ABONNEMENTEN, userId));

            while (this.db.getResult().next())
            {
                Abonnement abonnement = resultToAbonnement(this.db.getResult());
                abonnementen.add(abonnement);
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return abonnementen;
    }

    private Abonnement resultToAbonnement(ResultSet result) throws SQLException
    {
        int abonnementId = result.getInt("abonnementId");
        String startDatum = result.getString("startdatum");
        boolean verdubbeld = result.getBoolean("verdubbeld");
        String status = result.getString("status");
        boolean eigenaar = result.getBoolean("eigenaar");

        return new Abonnement(abonnementId, startDatum, verdubbeld, status, eigenaar, resultToDienst(result));
    }

    private Dienst resultToDienst(ResultSet result) throws SQLException
    {
        int dienstId = result.getInt("dienstId");
        String aanbieder = result.getString("aanbieder");
        String dienstNaam = result.getString("naam");
        double prijsPerMaand = result.getDouble("prijsPerMaand");
        int deelbaar = result.getInt("deelbaar");
        boolean verdubbelbaar = result.getBoolean("verdubbelbaar");

        return new Dienst(dienstId, aanbieder, dienstNaam, prijsPerMaand, deelbaar, verdubbelbaar);
    }

    @Override
    public Abonnement getAbonnemtenById(int userId, int id)
    {
        Abonnement abonnement = null;

        try
        {
            this.db.connectAndSelect(format(QUERY_ABONNEMENT_BY_ID, userId, id));

            while (this.db.getResult().next())
            {
                abonnement = resultToAbonnement(this.db.getResult());
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return abonnement;
    }

    @Override
    public boolean addAbonnement(int userId, int dienstId)
    {
        return this.db.connectAndManipulate(format(QUERY_INSERT_ABONNEMENT, userId, dienstId));
    }

    @Override
    public void updateAbonnementen()
    {
        this.updateAbonnementToProef();

        this.updateAbonnementToOpgezegd();
    }

    private void updateAbonnementToOpgezegd()
    {
        this.db.connectAndManipulate(QUERY_UPDATE_ABONNEMENT_TO_OPGEZEGD);
    }

    private void updateAbonnementToProef()
    {
        this.db.connectAndManipulate(QUERY_UPDATE_ABONNEMENT_TO_PROEF);
    }

    @Override
    public boolean verdubbelAbonnement(int userId, int id)
    {
        return this.db.connectAndManipulate(format(QUERY_VERDUBBEL_ABONNEMENT, userId, id));
    }

    @Override
    public boolean terminateAbonnement(int userId, int id)
    {
        return this.db.connectAndManipulate(format(QUERY_TERMINATE_ABONNEMENT, userId, id));
    }
}

