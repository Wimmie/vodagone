package nl.willemvandam.vodagone.dao.implementation;

import nl.willemvandam.vodagone.dao.IAbonneeDao;
import nl.willemvandam.vodagone.dao.IAbonnementenDao;
import nl.willemvandam.vodagone.dao.IDatabase;
import nl.willemvandam.vodagone.dao.ITokenDao;
import nl.willemvandam.vodagone.model.Abonnee;
import nl.willemvandam.vodagone.model.Token;
import nl.willemvandam.vodagone.model.User;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static java.lang.String.format;

public class AbonneeDao implements IAbonneeDao
{
    private static final String QUERY_USER_LOGIN = "SELECT ID, NAME, EMAIL, (SELECT t.TOKEN FROM token t WHERE t.ID =" +
            " " +
            "a.ID) as token FROM abonnee a WHERE ID = (SELECT ID FROM user WHERE PASSWORD = '%1$s' AND USERNAME = " +
            "'%2$s')";
    private static final String QUERY_ALL_OTHER_ABONNEES = "SELECT * FROM abonnee WHERE ID != %1$d";
    private static final String QUERY_SHARE_ABONNEMENT = "INSERT INTO gedeeldabonnement (ABONNEMENTID, ABONNEEID) " +
            "SELECT a.ID, %2$d FROM abonnement a WHERE a.ID = %3$d AND a.ABONNEE = %1$d AND (SELECT d.DEELBAAR FROM " +
            "dienst d WHERE d.ID = a.DIENST) > (SELECT COUNT(*) FROM gedeeldabonnement g WHERE g.ABONNEMENTID = a.ID)";
    @Inject
    private IDatabase db;
    @Inject
    private IAbonnementenDao abonnementenDao;
    @Inject
    private ITokenDao tokenDao;

    @Override
    public User getUser(String username, String password)
    {
        abonnementenDao.updateAbonnementen();
        tokenDao.deleteExpiredTokens();

        User user = null;

        try
        {
            this.db.connectAndSelect(format(QUERY_USER_LOGIN, password, username));

            while (this.db.getResult().next())
            {
                Abonnee abonnee = resultToAbonnee(this.db.getResult());
                String token = this.db.getResult().getString("token");
                token = this.checkToken(token, abonnee.getId());
                user = new User(abonnee, token);
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return user;
    }

    private String checkToken(String token, int id)
    {
        if (token != null)
        {
            return token;
        }
        token = Token.generateToken();
        this.tokenDao.insertOrUpdateToken(token, id);
        return token;
    }

    @Override
    public ArrayList<Abonnee> getAllOtherAbonnees(int userId)
    {
        ArrayList<Abonnee> abonnees = new ArrayList<>();

        try
        {
            this.db.connectAndSelect(format(QUERY_ALL_OTHER_ABONNEES, userId));

            while (this.db.getResult().next())
            {
                Abonnee abonnee = resultToAbonnee(this.db.getResult());
                abonnees.add(abonnee);
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return abonnees;
    }

    private Abonnee resultToAbonnee(ResultSet result) throws SQLException
    {
        int id = result.getInt("id");
        String name = result.getString("name");
        String email = result.getString("email");
        return new Abonnee(id, name, email);
    }

    @Override
    public boolean shareAbonnement(int ownerId, int abonneeId, int abonnementId)
    {
        return this.db.connectAndManipulate(format(QUERY_SHARE_ABONNEMENT, ownerId, abonneeId, abonnementId));
    }
}
