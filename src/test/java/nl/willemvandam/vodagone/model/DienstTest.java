package nl.willemvandam.vodagone.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DienstTest
{
    private final double DELTA = 0.001;
    private Dienst dienst;
    private final int id = 0;
    private final int deelbaar = 2;
    private final String aanbieder = "ziggo";
    private final String dienstNaam = "Internet";
    private final double prijsPerMaand = 5.00;
    private final boolean verdubbelbaar = true;

    @Before
    public void init()
    {
        this.dienst = new Dienst(this.id, this.aanbieder, this.dienstNaam, this.prijsPerMaand, this.deelbaar, this.verdubbelbaar);
    }

    @Test
    public void shouldReturnId()
    {
        assertEquals(this.id, this.dienst.getId());
    }

    @Test
    public void shouldReturnAanbieder()
    {
        assertEquals(this.aanbieder, this.dienst.getAanbieder());
    }

    @Test
    public void shouldReturnDienstNaam()
    {
        assertEquals(this.dienstNaam, this.dienst.getDienst());
    }

    @Test
    public void shouldReturnPrijsPerMaand()
    {
        assertEquals(this.prijsPerMaand, this.dienst.getPrijsPerMaand(), DELTA);
    }

    @Test
    public void shouldReturnAmountOfUsersDeelbaar()
    {
        assertEquals(this.deelbaar, this.dienst.getDeelbaar());
    }

    @Test
    public void shouldReturnVerdubbelbaar()
    {
        assertEquals(this.verdubbelbaar, this.dienst.getVerdubbelbaar());
    }
}