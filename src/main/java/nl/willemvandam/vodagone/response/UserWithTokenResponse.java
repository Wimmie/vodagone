package nl.willemvandam.vodagone.response;

import nl.willemvandam.vodagone.model.User;

public class UserWithTokenResponse
{
    private final String user;
    private final String token;

    public UserWithTokenResponse(User user)
    {
        this.user = user.getUser();
        this.token = user.getToken();
    }
}
