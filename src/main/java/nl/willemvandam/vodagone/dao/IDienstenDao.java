package nl.willemvandam.vodagone.dao;

import nl.willemvandam.vodagone.model.Dienst;

import java.util.ArrayList;

public interface IDienstenDao
{
    ArrayList<Dienst> getAllDiensten();
}
