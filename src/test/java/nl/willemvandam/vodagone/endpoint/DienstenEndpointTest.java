package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IDienstenDao;
import nl.willemvandam.vodagone.model.Dienst;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DienstenEndpointTest
{
    @Mock
    private IDienstenDao mockDienstenDao;

    @Mock
    private Auth mockAuth;

    @InjectMocks
    private DienstenEndpoint dienstenEndpoint;

    private ArrayList<Dienst> dienstenList;

    @Before
    public void init()
    {
        this.dienstenList = new ArrayList<>();
        this.dienstenList.add(new Dienst(0, "aanvieder", "dienst", 5.00, 2, true));
    }

    @Test
    public void shouldReturnForbiddenForGetDiensten()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = dienstenEndpoint.getDiensten("wrong_token", "filter");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnOkForGetDiensten()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockDienstenDao.getAllDiensten()).thenReturn(this.dienstenList);

        Response response = dienstenEndpoint.getDiensten("token", "filter");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnDienstenListForGetDiensten()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockDienstenDao.getAllDiensten()).thenReturn(this.dienstenList);

        Response response = dienstenEndpoint.getDiensten("token", "filter");

        assertEquals(this.dienstenList.getClass(), response.getEntity().getClass());
    }
}