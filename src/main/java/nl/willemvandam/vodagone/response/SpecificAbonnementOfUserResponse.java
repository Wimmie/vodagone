package nl.willemvandam.vodagone.response;

import nl.willemvandam.vodagone.model.Abonnement;

public class SpecificAbonnementOfUserResponse
{
    private final int id;
    private final String aanbieder;
    private final String dienst;
    private final String prijs;
    private final String startDatum;
    private final String verdubbeling;
    private final boolean deelbaar;
    private final String status;

    public SpecificAbonnementOfUserResponse(Abonnement abonnement)
    {
        this.id = abonnement.getId();
        this.aanbieder = abonnement.getAanbieder();
        this.dienst = abonnement.getDienstName();
        this.prijs = abonnement.getPrijsInString();
        this.startDatum = abonnement.getStartDatum();
        this.verdubbeling = abonnement.getVerdubbeling();
        this.deelbaar = abonnement.getDeelbaar();
        this.status = abonnement.getStatus();
    }
}
