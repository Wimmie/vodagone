package nl.willemvandam.vodagone.dao;

import nl.willemvandam.vodagone.model.Abonnee;
import nl.willemvandam.vodagone.model.User;

import java.util.ArrayList;

public interface IAbonneeDao
{
    User getUser(String username, String password);

    ArrayList<Abonnee> getAllOtherAbonnees(int userId);

    boolean shareAbonnement(int ownerId, int abonneeId, int abonnementId);
}
