package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.dao.IAbonneeDao;
import nl.willemvandam.vodagone.model.User;
import nl.willemvandam.vodagone.response.UserLogin;
import nl.willemvandam.vodagone.response.UserWithTokenResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginEndpoint
{
    @Inject
    private IAbonneeDao loginDao;

    @Path("login")
    @POST
    public Response login(UserLogin userLogin)
    {
        User user = loginDao.getUser(userLogin.getUser(), userLogin.getPassword());

        if (user == null)
        {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        return Response
                .status(Response.Status.CREATED)
                .entity(new UserWithTokenResponse(user))
                .build();
    }
}
