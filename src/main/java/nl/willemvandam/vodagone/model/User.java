package nl.willemvandam.vodagone.model;

public class User
{
    private String token;
    private Abonnee abonnee;

    public User(){}

    public User(Abonnee abonnee, String token)
    {
        this.abonnee = abonnee;
        this.token = token;
    }

    public String getToken()
    {
        return this.token;
    }

    public String getUser()
    {
        return this.abonnee.getName();
    }
}
