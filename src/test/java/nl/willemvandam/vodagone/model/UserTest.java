package nl.willemvandam.vodagone.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest
{
    private User user;

    @Before
    public void init()
    {
        Abonnee abonnee = new Abonnee(0, "Henk", "henk@westbroek.nl");
        this.user = new User(abonnee, "token");
    }

    @Test
    public void shouldReturnToken()
    {
        assertEquals("token", this.user.getToken());
    }

    @Test
    public void shouldReturnNameOfUser()
    {
        assertEquals("Henk", this.user.getUser());
    }
}