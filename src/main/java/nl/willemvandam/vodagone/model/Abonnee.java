package nl.willemvandam.vodagone.model;

public class Abonnee
{
    private final int id;
    private final String name;
    private final String email;

    public Abonnee(int id, String name, String email)
    {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getName()
    {
        return this.name;
    }

    public int getId()
    {
        return this.id;
    }
}
