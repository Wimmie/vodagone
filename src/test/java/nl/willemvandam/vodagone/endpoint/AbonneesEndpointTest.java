package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IAbonneeDao;
import nl.willemvandam.vodagone.model.Abonnee;
import nl.willemvandam.vodagone.response.AbonnementId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbonneesEndpointTest
{
    @Mock
    private IAbonneeDao mockAbonneeDao;

    @Mock
    private Auth mockAuth;

    @InjectMocks
    private AbonneesEndpoint abonneesEndpoint;

    @Mock
    private AbonnementId mockAbonnementId;

    @Test
    public void shouldReturnOkForAllOtherAbonnees()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonneesEndpoint.getAllOtherAbonnees("token");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnForbiddenForAllOtherAbonnees()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonneesEndpoint.getAllOtherAbonnees("wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnAbonneeForAllOtherAbonnees()
    {
        ArrayList<Abonnee> otherAbonneesList = new ArrayList<>();
        otherAbonneesList.add(new Abonnee(0, "Henk", "henk@westbroek.nl"));

        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAuth.getUserId()).thenReturn(1);
        when(mockAbonneeDao.getAllOtherAbonnees(1)).thenReturn(otherAbonneesList);

        Response response = abonneesEndpoint.getAllOtherAbonnees("token");

        assertEquals(otherAbonneesList.getClass(), response.getEntity().getClass());
    }

    @Test
    public void shouldReturnForbiddenForShareAbonnement()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonneesEndpoint.shareAbonnementenById(0, mockAbonnementId, "wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnBadRequestForShareAbonnement()
    {
        when(mockAbonnementId.getId()).thenReturn(0);
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonneeDao.shareAbonnement(anyInt(), anyInt(), anyInt())).thenReturn(false);

        Response response = abonneesEndpoint.shareAbonnementenById(0, mockAbonnementId, "token");

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnOkForShareAbonnement()
    {
        when(mockAbonnementId.getId()).thenReturn(0);
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonneeDao.shareAbonnement(anyInt(), anyInt(), anyInt())).thenReturn(true);

        Response response = abonneesEndpoint.shareAbonnementenById(0, mockAbonnementId, "token");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
}