package nl.willemvandam.vodagone.dao.implementation.util;

import java.io.IOException;
import java.util.Properties;

public class DatabaseProperties
{
    private Properties properties;

    public DatabaseProperties() {
        properties = new Properties();
        try
        {
            properties.load(getClass().getClassLoader().getResourceAsStream("database.properties"));
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public String server()
    {
        return properties.getProperty("server");
    }

    public String database()
    {
        return properties.getProperty("database");
    }

    public String username()
    {
        return properties.getProperty("username");
    }

    public String password()
    {
        return properties.getProperty("password");
    }

    public String driver()
    {
        return properties.getProperty("driver");
    }

    public String connectionString()
    {
        return properties.getProperty("connectionString");
    }
}
