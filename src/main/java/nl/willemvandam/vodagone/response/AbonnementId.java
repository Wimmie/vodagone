package nl.willemvandam.vodagone.response;

// Used to catch the abonnement ID that is given in the form request
public class AbonnementId
{
    private int id;

    public AbonnementId()
    {
    }

    public int getId()
    {
        return this.id;
    }
}
