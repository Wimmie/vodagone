package nl.willemvandam.vodagone.response;

public class DienstResponse
{
    private int id;
    private String aanbieder;
    private String dienst;

    public DienstResponse()
    {
    }

    public DienstResponse(int id, String aanbieder, String dienst)
    {
        this.id = id;
        this.aanbieder = aanbieder;
        this.dienst = dienst;
    }

    public int getId()
    {
        return this.id;
    }
}
