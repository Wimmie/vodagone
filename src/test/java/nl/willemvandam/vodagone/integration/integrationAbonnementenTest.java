package nl.willemvandam.vodagone.integration;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import nl.willemvandam.vodagone.response.DienstResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class integrationAbonnementenTest
{
    private String token;

    @Before
    public void init()
    {
        RestAssured.port = 8080;

        String json = "{\"user\": \"w.vandam2@student.han.nl\",\"password\": \"1995-12-06\"}";

        // Get the token of the user
        token =
                given().
                        accept(ContentType.JSON).
                        contentType(ContentType.JSON).
                        body(json).
                when().
                        post("/login").
                then().
                extract().
                        path("token");
    }

    @Test
    public void shouldReturnAbonnementenForAbonnementenOfUser()
    {
        given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen").
        then().
                body(matchesJsonSchemaInClasspath("abonnementen-overview-schema.json"));
    }

    @Test
    public void shouldReturnAbonnementenForAddAbonnement()
    {
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(1, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen").
        then().
                body(matchesJsonSchemaInClasspath("abonnementen-overview-schema.json"));
    }

    @Test
    public void shouldReturnSpecificAbonnementForAbonenmentById()
    {
        given().
                pathParam("id", 1).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body(matchesJsonSchemaInClasspath("specific-abonnement-schema.json"));
    }

    @Test
    public void shouldReturnSpecificAbonnementForSharedAbonenmentById()
    {
        // Abonnement 4 is shared by user 2 with user 1
        given().
                pathParam("id", 4).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body(matchesJsonSchemaInClasspath("specific-abonnement-schema.json"));
    }

    // Test will fail when there are more then 44 abonnementen.
    @Test
    public void shouldReturnSpecificAbonnementForVerdubbelAbonnement()
    {
        // Insert an abonnement that can be verdubbeld
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(3, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen");

        // Get the ID of the inserted abonnement
        ArrayList<Integer> ids = given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen").
        then().
        extract().
                path("abonnementen.id");
        int id = ids.get(ids.size() - 1);

        // Check that the abonnement is not yet verdubbeld and get the price
        String nietVerdubbeldPrice = given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("verdubbeling", is("standaard")).
        extract().
                path("prijs");

        // Verdubbel the abonnement
        given().
                contentType(ContentType.JSON).
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen/{id}").
        then().
                body("verdubbeling", is("verdubbeld")).
                body("prijs", not(is(nietVerdubbeldPrice))).
                body(matchesJsonSchemaInClasspath("specific-abonnement-schema.json"));
    }

    @Test
    public void shouldReturnSpecificAbonnementForTerminateAbonnement()
    {
        // Insert an abonnement that will be terminated
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(1, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.CREATED.getStatusCode()).
        when().
                post("/abonnementen");

        // Get the ID of the inserted abonnement
        ArrayList<Integer> ids = given().
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen").
        then().
        extract().
                path("abonnementen.id");
        int id = ids.get(ids.size() - 1);

        // Check that the abonnement is not yet terminated and get the price
        String notTerminatedPrice = given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                get("/abonnementen/{id}").
        then().
                body("status", not(is("opgezegd"))).
        extract().
                path("prijs");

        // Terminate the abonnement
        given().
                pathParam("id", id).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.OK.getStatusCode()).
        when().
                delete("/abonnementen/{id}").
        then().
                body("status", is("opgezegd")).
                body("prijs", not(is(notTerminatedPrice))).
                body(matchesJsonSchemaInClasspath("specific-abonnement-schema.json"));
    }

    // All the bad requests
    @Test
    public void shouldReturnBadRequestForTerminatingNotOwnedAbonnement()
    {
        given().
                pathParam("id", 4).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                delete("/abonnementen/{id}");
    }

    @Test
    public void shouldReturnBadRequestForVerdubbelenNotOwnedAbonnement()
    {
        given().
                contentType(ContentType.JSON).
                pathParam("id", 4).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                post("/abonnementen/{id}");
    }

    @Test
    public void shouldReturnBadRequestForTerminatingNotExistingAbonnement()
    {
        given().
                pathParam("id", -1).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                delete("/abonnementen/{id}");
    }

    @Test
    public void shouldReturnBadRequestForVerdubbelenNotExistingAbonnement()
    {
        given().
                contentType(ContentType.JSON).
                pathParam("id", -1).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                post("/abonnementen/{id}");
    }

    @Test
    public void shouldReturnBadRequestForAddNotExistingAbonnement()
    {
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(-1, "aanbieder", "dienst")).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                post("/abonnementen");
    }

    @Test
    public void shouldReturnBadRequestForAbonnementByNotOwnedOrSharedId()
    {
        given().
                pathParam("id", -1).
                queryParam("token", token).
        expect().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode()).
        when().
                get("/abonnementen/{id}");
    }

    // All the forbidden requests
    @Test
    public void shouldReturnForbiddenForBadTokenAbonnementOfUser()
    {
        given().
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                get("/abonnementen");
    }

    @Test
    public void shouldReturnForbiddenForBadTokenAddAbonnement()
    {
        given().
                accept(ContentType.JSON).
                contentType(ContentType.JSON).
                body(new DienstResponse(1, "aanbieder", "dienst")).
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                post("/abonnementen");
    }

    @Test
    public void shouldReturnForbiddenForBadTokenAbonnementById()
    {
        given().
                queryParam("id", 1).
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                get("/abonnementen");
    }

    @Test
    public void shouldReturnForbiddenForBadTokenVerdubbelAbonnementById()
    {
        given().
                contentType(ContentType.JSON).
                pathParam("id", 1).
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                post("/abonnementen/{id}");
    }

    @Test
    public void shouldReturnForbiddenForBadTokenTerminateAbonnementById()
    {
        given().
                pathParam("id", 1).
                queryParam("token", "token").
        expect().
                statusCode(Response.Status.FORBIDDEN.getStatusCode()).
        when().
                delete("/abonnementen/{id}");
    }
}
