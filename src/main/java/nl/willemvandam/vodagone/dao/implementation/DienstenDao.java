package nl.willemvandam.vodagone.dao.implementation;

import nl.willemvandam.vodagone.dao.IDatabase;
import nl.willemvandam.vodagone.dao.IDienstenDao;
import nl.willemvandam.vodagone.model.Dienst;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DienstenDao implements IDienstenDao
{
    @Inject
    private IDatabase db;

    private static final String QUERY_ALL_DIENSTEN = "SELECT d.ID, d.AANBIEDER, d.NAAM, (SELECT p.PRIJS FROM prijzen " +
            "p WHERE p.DIENST = d.ID AND p.LENGTE = 'maand') prijsPerMaand, d.DEELBAAR, d.VERDUBBELBAAR\n" +
            "FROM dienst d";

    @Override
    public ArrayList<Dienst> getAllDiensten()
    {
        ArrayList<Dienst> diensten = new ArrayList<>();

        try
        {
            this.db.connectAndSelect(QUERY_ALL_DIENSTEN);

            while (this.db.getResult().next())
            {
                Dienst dienst = resultToDienst(this.db.getResult());
                diensten.add(dienst);
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return diensten;
    }

    private Dienst resultToDienst(ResultSet result) throws SQLException
    {
        int dienstId = result.getInt("id");
        String aanbieder = result.getString("aanbieder");
        String dienstNaam = result.getString("naam");
        double prijsPerMaand = result.getDouble("prijsPerMaand");
        int deelbaar = result.getInt("deelbaar");
        boolean verdubbelbaar = result.getBoolean("verdubbelbaar");
        return new Dienst(dienstId, aanbieder, dienstNaam, prijsPerMaand, deelbaar, verdubbelbaar);
    }
}
