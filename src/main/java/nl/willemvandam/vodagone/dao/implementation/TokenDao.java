package nl.willemvandam.vodagone.dao.implementation;

import nl.willemvandam.vodagone.dao.IDatabase;
import nl.willemvandam.vodagone.dao.ITokenDao;

import javax.inject.Inject;
import java.sql.SQLException;

import static java.lang.String.format;

public class TokenDao implements ITokenDao
{
    @Inject
    private IDatabase db;

    private static final String QUERY_DELETE_EXPIRED_TOKEN = "DELETE FROM token WHERE EXPIRES < NOW()";
    private static final String QUERY_INSERT_OR_UPDATE_TOKEN = "INSERT INTO token VALUES('%1$s', %2$d, NOW() + " +
            "INTERVAL 1 DAY) ON DUPLICATE KEY UPDATE token = '%1$s', expires = NOW() + INTERVAL 1 DAY";
    private static final String QUERY_GET_ID_BY_TOKEN = "SELECT t.ID FROM token t WHERE t.TOKEN = '%1$s'";

    @Override
    public void deleteExpiredTokens()
    {
        this.db.connectAndManipulate(QUERY_DELETE_EXPIRED_TOKEN);
    }

    @Override
    public void insertOrUpdateToken(String token, int abonneeId)
    {
        this.db.connectAndManipulate(format(QUERY_INSERT_OR_UPDATE_TOKEN, token, abonneeId));
    }

    @Override
    public int getIdByToken(String token)
    {
        int id = -1;

        try
        {
            this.db.connectAndSelect(format(QUERY_GET_ID_BY_TOKEN, token));

            while (this.db.getResult().next())
            {
                id = this.db.getResult().getInt("ID");
            }
        }
        catch (SQLException ex)
        {
            this.db.printSQLExeption(ex);
        }
        finally
        {
            this.db.closeConnection();
        }
        return id;
    }
}
