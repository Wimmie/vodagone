package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IAbonnementenDao;
import nl.willemvandam.vodagone.model.Abonnement;
import nl.willemvandam.vodagone.response.AbonnementenOfUserResponse;
import nl.willemvandam.vodagone.response.DienstResponse;
import nl.willemvandam.vodagone.response.SpecificAbonnementOfUserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbonnementenEndpointTest
{
    @Mock
    private IAbonnementenDao mockAbonnementenDao;

    @Mock
    private Auth mockAuth;

    @InjectMocks
    private AbonnementenEndpoint abonnementenEndpoint;

    @Test
    public void shouldReturnForbiddenForAbonnementenOfUser()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonnementenEndpoint.getAbonnementenOfUser("wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnOkForAbonnementenOfUser()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonnementenEndpoint.getAbonnementenOfUser("token");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnAbonnementenOfUserResponseForAbonnementenOfUser()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonnementenEndpoint.getAbonnementenOfUser("token");

        assertEquals(AbonnementenOfUserResponse.class, response.getEntity().getClass());
    }

    @Test
    public void shouldReturnForbiddenForAddAbonnement()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonnementenEndpoint.addAbonnement("wrong_token", new DienstResponse());

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnBadRequestForAddAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonnementenEndpoint.addAbonnement("token", new DienstResponse());

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnCreatedForAddAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.addAbonnement(anyInt(), anyInt())).thenReturn(true);

        Response response = abonnementenEndpoint.addAbonnement("token", new DienstResponse());

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnAbonnementenOfUserResponseForAddAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.addAbonnement(anyInt(), anyInt())).thenReturn(true);

        Response response = abonnementenEndpoint.addAbonnement("token", new DienstResponse());

        assertEquals(AbonnementenOfUserResponse.class, response.getEntity().getClass());
    }

    @Test
    public void shouldReturnForbiddenForAbonnementById()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonnementenEndpoint.getAbonnementById(0, "wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnOkForAbonnementById()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.getAbonnementById(0, "token");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnSpecificAbonnementResponseForAbonnementById()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.getAbonnementById(0, "token");

        assertEquals(SpecificAbonnementOfUserResponse.class, response.getEntity().getClass());
    }

    @Test
    public void shouldReturnForbiddenForVerdubbelAbonnement()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonnementenEndpoint.verdubbelAbonnementById(0, "wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnBadRequestForVerdubbelAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonnementenEndpoint.verdubbelAbonnementById(0, "token");

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnCreatedForVerdubbelAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.verdubbelAbonnement(anyInt(), anyInt())).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.verdubbelAbonnementById(0, "token");

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnSpecificAbonnementForVerdubbelAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.verdubbelAbonnement(anyInt(), anyInt())).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.verdubbelAbonnementById(0, "token");

        assertEquals(SpecificAbonnementOfUserResponse.class, response.getEntity().getClass());
    }

    @Test
    public void shouldReturnForbiddenForTerminateAbonnement()
    {
        when(mockAuth.isAuthorized("wrong_token")).thenReturn(false);

        Response response = abonnementenEndpoint.terminateAbonnementById(0, "wrong_token");

        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnBadRequestForTerminateAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);

        Response response = abonnementenEndpoint.terminateAbonnementById(0, "token");

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnOkForTerminateAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.terminateAbonnement(anyInt(), anyInt())).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.terminateAbonnementById(0, "token");

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldReturnSpecificAbonnementForTerminateAbonnement()
    {
        when(mockAuth.isAuthorized("token")).thenReturn(true);
        when(mockAbonnementenDao.terminateAbonnement(anyInt(), anyInt())).thenReturn(true);
        when(mockAbonnementenDao.getAbonnemtenById(anyInt(), anyInt())).thenReturn(mock(Abonnement.class));

        Response response = abonnementenEndpoint.terminateAbonnementById(0, "token");

        assertEquals(SpecificAbonnementOfUserResponse.class, response.getEntity().getClass());
    }
}