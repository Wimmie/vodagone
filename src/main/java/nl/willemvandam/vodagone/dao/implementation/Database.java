package nl.willemvandam.vodagone.dao.implementation;

import nl.willemvandam.vodagone.dao.IDatabase;
import nl.willemvandam.vodagone.dao.implementation.util.DatabaseProperties;

import java.sql.*;

public class Database implements IDatabase
{
    private static Connection connection;
    private Statement stmt;
    private ResultSet result;
    private String query;

    public Database()
    {
        if (connection != null)
        {
            return;
        }
        DatabaseProperties dp = new DatabaseProperties();
        String connectionString = String.format(dp.connectionString(), dp.server(), dp.database(), dp.username(), dp.password());
        this.initConnection(connectionString, dp.driver());
    }

    private void initConnection(String connectionString, String driver)
    {
        this.result = null;
        this.stmt = null;
        this.registerDriver(driver);

        try
        {
            connection = DriverManager.getConnection(connectionString);
        }
        catch (SQLException ex)
        {
            printSQLExeption(ex);
        }
    }

    @Override
    public void printSQLExeption(SQLException ex)
    {
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }

    private void registerDriver(String driver)
    {
        try
        {
            Class.forName(driver).newInstance();
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void connectAndSelect(String query) throws SQLException
    {
        this.query = query;
        this.stmt = connection.createStatement();
        this.result = this.stmt.executeQuery(this.query);
    }

    private boolean connectAndManipulate()
    {
        boolean success = false;
        try
        {
            this.stmt = connection.createStatement();
            this.stmt.executeUpdate(this.query);

            if (this.stmt.getUpdateCount() > 0)
            {
                success = true;
            }
        }
        catch (SQLException ex)
        {
            this.printSQLExeption(ex);
        }
        finally
        {
            this.closeConnection();
        }
        return success;
    }

    @Override
    public boolean connectAndManipulate(String query)
    {
        this.query = query;
        return this.connectAndManipulate();
    }

    @Override
    public void closeConnection()
    {
        this.closeResult();
        this.closeStmt();
    }

    private void closeStmt()
    {
        if (this.stmt != null)
        {
            try
            {
                this.stmt.close();
            }
            catch (SQLException ignored)
            {
            }

            this.stmt = null;
        }
    }

    private void closeResult()
    {
        if (this.result != null)
        {
            try
            {
                this.result.close();
            }
            catch (SQLException ignored)
            {
            }

            this.result = null;
        }
    }

    @Override
    public ResultSet getResult()
    {
        return this.result;
    }
}
