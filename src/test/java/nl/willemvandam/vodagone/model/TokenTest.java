package nl.willemvandam.vodagone.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TokenTest
{
    @Test
    public void shouldReturnToken19Long()
    {
        assertEquals(19, Token.generateToken().length());
    }

    @Test
    public void shouldReturnDifferentTokens()
    {
        //This test can fail, but the chance is extremely small
        assertNotEquals(Token.generateToken(), Token.generateToken());
    }
}