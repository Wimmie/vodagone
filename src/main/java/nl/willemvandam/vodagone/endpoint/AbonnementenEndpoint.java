package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IAbonnementenDao;
import nl.willemvandam.vodagone.model.Abonnement;
import nl.willemvandam.vodagone.response.AbonnementenOfUserResponse;
import nl.willemvandam.vodagone.response.DienstResponse;
import nl.willemvandam.vodagone.response.SpecificAbonnementOfUserResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/abonnementen")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbonnementenEndpoint
{
    @Inject
    private IAbonnementenDao abonnementenDao;

    @Inject
    private Auth auth;

    @Path("")
    @GET
    public Response getAbonnementenOfUser(@QueryParam("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        return Response.ok()
                .entity(getAbonnementen(auth.getUserId()))
                .build();
    }

    @Path("")
    @POST
    public Response addAbonnement(@QueryParam("token") String token, DienstResponse dienst)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        boolean success = abonnementenDao.addAbonnement(auth.getUserId(), dienst.getId());

        if (!success)
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.status(Response.Status.CREATED)
                .entity(getAbonnementen(auth.getUserId()))
                .build();
    }

    @Path("/{id}")
    @GET
    public Response getAbonnementById(@PathParam("id") int id, @QueryParam("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        SpecificAbonnementOfUserResponse specificAbonnement = getSpecificAbonnement(id, auth.getUserId());

        if (specificAbonnement == null)
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.ok()
                .entity(specificAbonnement)
                .build();
    }

    @Path("/{id}")
    @POST
    public Response verdubbelAbonnementById(@PathParam("id") int id, @QueryParam("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        boolean success = abonnementenDao.verdubbelAbonnement(auth.getUserId(), id);

        if (!success)
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.status(Response.Status.CREATED)
                .entity(getSpecificAbonnement(id, auth.getUserId()))
                .build();
    }

    @Path("/{id}")
    @DELETE
    public Response terminateAbonnementById(@PathParam("id") int id, @QueryParam("token") String token)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        boolean success = abonnementenDao.terminateAbonnement(auth.getUserId(), id);

        if (!success)
        {
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

        return Response.ok()
                .entity(getSpecificAbonnement(id, auth.getUserId()))
                .build();
    }

    private SpecificAbonnementOfUserResponse getSpecificAbonnement(int id, int userId)
    {
        Abonnement abonnemten = abonnementenDao.getAbonnemtenById(userId, id);

        if (abonnemten == null)
        {
            return null;
        }

        return new SpecificAbonnementOfUserResponse(abonnemten);
    }

    private AbonnementenOfUserResponse getAbonnementen(int userId)
    {
        return new AbonnementenOfUserResponse(abonnementenDao.getAllAbonnementen(userId));
    }
}
