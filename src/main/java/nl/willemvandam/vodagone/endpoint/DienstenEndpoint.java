package nl.willemvandam.vodagone.endpoint;

import nl.willemvandam.vodagone.authentication.Auth;
import nl.willemvandam.vodagone.dao.IDienstenDao;
import nl.willemvandam.vodagone.model.Dienst;
import nl.willemvandam.vodagone.response.DienstResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/abonnementen")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DienstenEndpoint
{
    @Inject
    private IDienstenDao dienstenDao;

    @Inject
    private Auth auth;

    @Path("/all")
    @GET
    public Response getDiensten(@QueryParam("token") String token, @QueryParam("filter") String filter)
    {
        if (!auth.isAuthorized(token)) return Response.status(Response.Status.FORBIDDEN).build();

        ArrayList<DienstResponse> dienstenResponse = getDienstResponse(filter);

        return Response.ok()
                .entity(dienstenResponse)
                .build();
    }

    private ArrayList<DienstResponse> getDienstResponse(String filter)
    {
        ArrayList<DienstResponse> dienstenResponse = new ArrayList<>();

        for (Dienst dienst : dienstenDao.getAllDiensten())
        {
            if (dienst.getDienst().contains(filter) || dienst.getAanbieder().contains(filter))
            {
                dienstenResponse.add(new DienstResponse(dienst.getId(), dienst.getAanbieder(), dienst.getDienst()));
            }
        }
        return dienstenResponse;
    }
}
